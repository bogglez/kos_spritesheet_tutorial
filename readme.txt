This tutorial was written by bogglez for the dcemulation.org development wiki.

The following artists were so kind to provide artwork under the "Creative Commons by attribution" license:

Stephen "Redshrike" Challener: http://opengameart.org/content/bosses-and-monsters-spritesheets-ars-notoria

Ravenmore (dycha.net): http://opengameart.org/content/fantasy-ui-elements-by-ravenmore
