#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <arch/timer.h>
#include <dc/pvr.h>


// Extra output
#define VERBOSE


/* Files contained in the romdisk are access as "/rd/foo.png" */
extern uint8 romdisk[];
KOS_INIT_ROMDISK(romdisk);


/* In this tutorial tvspelfreak's DTEX texture container format is used. It's similar to KOS' KMG.
 * The contained data consistis of 8-bit paletted images here (see texconv call in Makefile).
 * It is documented here: https://github.com/tvspelsfreak/texconv
 * */
struct dtex_header {
	uint8_t  magic[4]; // 'DTEX'
	uint16_t width;
	uint16_t height;
	uint32_t type;
	uint32_t size;
};

/* DTEX .pal palette files also have a header (this is from romdisk/foo.dtex.pal) */
struct dpal_header {
	uint8_t  magic[4]; // 'DPAL'
	uint32_t color_count;
};

/* Offset and dimensions of each sprite within a spritesheet (romdisk/foo.txt file) */
struct sprite {
	char name[32];
	uint16_t x, y, width, height;
};

struct spritesheet {
	uint16_t width;         /* Dimensions of texture to compute UV coordinates of sprites */
	uint16_t height;        /* later for drawing.                                         */
	pvr_ptr_t texture;      /* Pointer to texture in video memory,   from file *.dtex     */
	uint32_t palette[256];  /* Palette of texture, up to 256 colors, from file *.dtex.pal */
	uint16_t color_count;   /* How many colors are actually used in the palette           */
	uint16_t sprite_count;  /* Amount of sprites in spritesheet,     from file *.txt      */
	struct sprite *sprites; /* Information for drawing each sprite,  from file *.txt      */
};


/* Spritesheets are global variables in this tutorial
 * E.g. ui_sheet will contain romdisk/ui_sheet.dtex,
 *   which has been converted from build/ui_sheet.png,
 *   which has been generated from assets/spritesheets/ui/.
 */
struct spritesheet stage1_actors_sheet, ui_sheet;


/* This function will load a spritesheet texture, the info about each sprite contained within
 * (from the corresponding txt file) and the palette (from the pal file).
 */
static int spritesheet_load(
		struct spritesheet * const spritesheet, /* Where to store the loaded information */
		char const * const image_filename,      /* foo.dtex     */
		char const * const palette_filename,    /* foo.dtex.pal */
		char const * const sheet_filename       /* foo.txt      */
	) {
	int result = 0;
	FILE *image_file       = NULL;
	FILE *palette_file     = NULL;
	FILE *sheet_file       = NULL;
	pvr_ptr_t texture      = NULL;
	struct sprite *sprites = NULL;


	/* Open all files */
	image_file   = fopen(image_filename,   "rb");
	palette_file = fopen(palette_filename, "rb");
	sheet_file   = fopen(sheet_filename,   "rb");

	if(!image_file | !palette_file | !sheet_file) {
		result = 1;
		goto cleanup;
	}


	/* Read DTEX texture */
	struct dtex_header dtex_header;
	if(fread(&dtex_header, sizeof(dtex_header), 1, image_file) != 1) {
		result = 2;
		goto cleanup;
	}

	/* Only allow 8 bit palette dtex files */
	if(memcmp(dtex_header.magic, "DTEX", 4) || dtex_header.type != 0x30000000) {
		result = 3;
		goto cleanup;
	}

	texture = pvr_mem_malloc(dtex_header.size);
	if(!texture) {
		result = 4;
		goto cleanup;
	}

	if(fread(texture, dtex_header.size, 1, image_file) != 1) {
		result = 5;
		goto cleanup;
	}


	/* Read DTEX palette */
	struct dpal_header dpal_header;
	if(fread(&dpal_header, sizeof(dpal_header), 1, palette_file) != 1) {
		result = 6;
		goto cleanup;
	}

	if(memcmp(dpal_header.magic, "DPAL", 4) | !dpal_header.color_count) {
		result = 7;
		goto cleanup;
	}

	if(fread(spritesheet->palette, sizeof(uint32_t), dpal_header.color_count, palette_file) != dpal_header.color_count) {
		result = 8;
		goto cleanup;
	}


	/* Read sprite offsets and dimensions from *.txt file */
	uint16_t sprite_count = 0;
	while(!feof(sheet_file)) { /* Determine sprite count by line count */
		if(fgetc(sheet_file) == '\n')
			++sprite_count;
		if(sprite_count == UINT16_MAX)
			break;
	}

	sprites = malloc(sprite_count * sizeof(struct sprite));
	if(!sprites) {
		result = 9;
		goto cleanup;
	}

	rewind(sheet_file);
	struct sprite * sprite = sprites;
	for(uint16_t i = 0; i < sprite_count && !feof(sheet_file); ++i) {
		int scanned = fscanf(sheet_file, "%[^,], %hu, %hu, %hu, %hu, 0, 0, 0, 0\n", sprite->name, &sprite->x, &sprite->y, &sprite->width, &sprite->height);
#ifdef VERBOSE
		printf("scanned %d parameters: %s %d %d %d %d\n", scanned, sprite->name, sprite->x, sprite->y, sprite->width, sprite->height);
#endif
		if(scanned != 5) {
			result = 10;
			goto cleanup;
		}

		++sprite;
	}


	/* Store info */
	spritesheet->width        = dtex_header.width;
	spritesheet->height       = dtex_header.height;
	spritesheet->texture      = texture;
	spritesheet->color_count  = dpal_header.color_count;
	spritesheet->sprite_count = sprite_count;
	spritesheet->sprites      = sprites;

cleanup:
	if(result) {
		if(image_file)   fclose(image_file);
		if(palette_file) fclose(palette_file);
		if(sheet_file)   fclose(sheet_file);
		if(texture)      pvr_mem_free(texture);
		free(sprites);
	}

	return result;
}

/* Free ressources used by spritesheet, if any. */
void spritesheet_free(struct spritesheet * const spritesheet) {
	if(!spritesheet) return;
	if(spritesheet->texture) pvr_mem_free(spritesheet->texture);
	if(spritesheet->sprites) free(spritesheet->sprites);
};

/* Initialize the game */
static int init() {
	int result = 0;

	/* Initialize the PVR graphics chip.
	 * Set how many polygons can be stored in each of the PVRpolygon lists.
	 * Only opaque and punchthru are enabled in this tutorial, only punchthru is used.
	 */
	pvr_init_params_t pvr_params = {
		.opb_sizes = { PVR_BINSIZE_8, PVR_BINSIZE_0, PVR_BINSIZE_0, PVR_BINSIZE_0, PVR_BINSIZE_8 },
		.vertex_buf_size = 512 * 1024
	};
	if(pvr_init(&pvr_params)) {
		result = 1;
		goto cleanup;
	}

	pvr_set_bg_color(0.3, 0.3, 0.3);


	/* Load spritesheets */
	memset(&stage1_actors_sheet, 0, sizeof(stage1_actors_sheet));
	memset(&ui_sheet,            0, sizeof(stage1_actors_sheet));

	result = spritesheet_load(&stage1_actors_sheet,
		"/rd/stage1_actors_sheet.dtex",
		"/rd/stage1_actors_sheet.dtex.pal",
		"/rd/stage1_actors_sheet.txt"
	);
	if(result) {
		printf("Cannot load stage1_actors spritesheet! Error %d\n", result);
		result = 1;
		goto cleanup;
	}

	result = spritesheet_load(&ui_sheet,
		"/rd/ui_sheet.dtex",
		"/rd/ui_sheet.dtex.pal",
		"/rd/ui_sheet.txt"
	);
	if(result) {
		printf("Cannot load ui spritesheet! Error %d\n", result);
		result = 2;
		goto cleanup;
	}

	printf("\nSpritesheets:\n  stage1_actors: %u sprites, %u colors\n  ui:            %u sprites, %u colors\n\n",
		stage1_actors_sheet.sprite_count, stage1_actors_sheet.color_count,
		ui_sheet.sprite_count,            ui_sheet.color_count
	);

cleanup:
	if(result) {
		spritesheet_free(&stage1_actors_sheet);
		spritesheet_free(&ui_sheet);
	}

	return result;
}

/* Before drawing a paletted sprite, the palette needs to be set. It persists throughout the whole frame.
 * So if you're using a lot of different palettes, you will have to manage where to put them.
 * palette_number=0 will use entries [0,256), =1 will use [256,512).
 */
void setup_palette(uint32_t const * colors, uint16_t count, uint8_t palette_number) {
	pvr_set_pal_format(PVR_PAL_ARGB8888);
	for(uint16_t i = 0; i < count; ++i)
		pvr_set_pal_entry(i + 256 * palette_number, colors[i]);
}

/* Draw a sprite at x, y using the given palette number. 'name' is the same as in spritesheet.sprites[i].name.
 * You can look up the available sprite names in romdisk/foo.txt.
 */
void draw_sprite(struct spritesheet const * const sheet, char const * const name, float x, float y, uint8_t palette_number) {
	/* Find sprite by name */
	uint16_t const sprite_count = sheet->sprite_count;
	struct sprite const * const sprites = sheet->sprites;
	struct sprite const * sprite = NULL;
	for(uint16_t i = 0; i < sprite_count; ++i) {
		if(!strcmp(sprites[i].name, name)) {
			sprite = &sprites[i];
			break;
		}
	}

	if(sprite == NULL) {
		printf("There is no sprite named '%s' in this spritesheet.\n", name);
		return;
	}

	/* Sprite geometry:
	 *
	 * (x0,y0,z)----(x1,y0,z)
	 *    |          |
	 * (x0,y1,z)----(x1,y1,z)
	 */
	float const x0 = x;
	float const y0 = y;
	float const x1 = x + sprite->width;
	float const y1 = y + sprite->height;
	float const z = 1;

	float const u0 =  sprite->x                   / (float)sheet->width;
	float const v0 =  sprite->y                   / (float)sheet->height;
	float const u1 = (sprite->x + sprite->width)  / (float)sheet->width;
	float const v1 = (sprite->y + sprite->height) / (float)sheet->height;


	/* Uncomment this to see sprite draw mode.
	 * Be aware that sprite draw mode uses 16 bit floats for texture coordinates,
	 * therefore you might suffer from precision problems! You can see this happen for
	 * one of the characters, because it it badly positioned in the spritesheet.
	 */
//	#define USE_SPRITE_DRAW_MODE

#ifndef USE_SPRITE_DRAW_MODE
	/* Create command for sending textured triangle strips with alpha masking. */
	pvr_poly_cxt_t context; /* This is just a convenience function for creating the following. */
	pvr_poly_hdr_t header;  /* This is sent to the PVR before geometry is sent. */

	/* Define which palette, texture and polyon list are to be used (punchthru for alpha masking here).
	 * Filtering is disabled because sprites are displayed 1:1.
	 */
	pvr_poly_cxt_txr(&context, PVR_LIST_PT_POLY, PVR_TXRFMT_PAL8BPP | PVR_TXRFMT_8BPP_PAL(palette_number), sheet->width, sheet->height, sheet->texture, PVR_FILTER_NONE);
	pvr_poly_compile(&header, &context);

	pvr_prim(&header, sizeof(header));

	/* Common attributes of the 4 vertices/points of the sprite. */
	pvr_vertex_t vert = {
		.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f),
		.oargb = 0,
		.flags = PVR_CMD_VERTEX, /* This is not the last point */
		.z = z
	};

	/* Now send the 4 points of the rectangle. Note that this is actually a "triangle strip".
	 * Sending each point with pvr_prim() is quite inefficient, so consider using the SH4's
	 * "store queues" for larger batches of sprites.
	 * The header also only needs to be sent once before drawing sprites that use the same texture.
	 */
    vert.x = x0;
    vert.y = y0;
    vert.u = u0;
    vert.v = v0;
    pvr_prim(&vert, sizeof(vert));

    vert.x = x1;
    vert.y = y0;
    vert.u = u1;
    vert.v = v0;
    pvr_prim(&vert, sizeof(vert));

    vert.x = x0;
    vert.y = y1;
    vert.u = u0;
    vert.v = v1;
    pvr_prim(&vert, sizeof(vert));

    vert.flags = PVR_CMD_VERTEX_EOL; /* Mark this point as the last. Next command would create a new polygon. */
    vert.x = x1;
    vert.y = y1;
    vert.u = u1;
    vert.v = v1;
    pvr_prim(&vert, sizeof(vert));

#else
	/* Create command for sending textured sprites with alpha masking.
	 * Instead of using 3D polygons the PVR's special "sprite" drawing mode is used.
	 * It is a special drawing mode for unrotated, uncolored rectangles with textures on them.
	 * If you want 3D effects and rotation, you will need to use pvr_poly_cxt_t etc.
	 * It is more efficient for sending large batches of sprites.
	 */
	pvr_sprite_cxt_t context; /* This is just a convenience function for creating the following. */
	pvr_sprite_hdr_t header;  /* This is sent to the PVR before geometry is sent. */

	/* Define which palette, texture and polyon list are to be used (punchthru for alpha masking here).
	 * Filtering is disabled because sprites are displayed 1:1.
	 */
	pvr_sprite_cxt_txr(&context, PVR_LIST_PT_POLY, PVR_TXRFMT_PAL8BPP | PVR_TXRFMT_8BPP_PAL(palette_number), sheet->width, sheet->height, sheet->texture, PVR_FILTER_NONE);
	pvr_sprite_compile(&header, &context);

	pvr_prim(&header, sizeof(header));

	pvr_sprite_txr_t vert = {
		.flags = PVR_CMD_VERTEX_EOL,
		.ax = x0,
		.ay = y0,
		.az = z,

		.bx = x1,
		.by = y0,
		.bz = z,

		.cx = x1,
		.cy = y1,
		.cz = z,

		.dx = x0,
		.dy = y1,
		.auv = PVR_PACK_16BIT_UV(u0, v0),
		.buv = PVR_PACK_16BIT_UV(u1, v0),
		.cuv = PVR_PACK_16BIT_UV(u1, v1),
	};

	/* Thanks to the sprite draw mode only one call is necessary.
	 * If you want to draw large batches of sprites you should use the SH4's "store queues" instead. */
    pvr_prim(&vert, sizeof(vert));
#endif
}


static void draw() {
	pvr_wait_ready(); /* Await v-blank */
	pvr_scene_begin();

	/* Start sending punchthru polygons. */
	pvr_list_begin(PVR_LIST_PT_POLY);

	/* The palette must be set up for the entire frame.
	 * This means that sprites share the palette, so be careful.
	 * You can exploit this for nice color animations!
	 * Here the user interface's palette is put in palette 0, entries [0, 256),
	 * the stage 1 actors' palette is put in palette 1, entries [256,512).
	 */
	uint8_t const ui_palette_number = 0;
	uint8_t const stage1_actors_palette_number = 1;
	setup_palette(ui_sheet.palette, ui_sheet.color_count, ui_palette_number);
	setup_palette(stage1_actors_sheet.palette, stage1_actors_sheet.color_count, stage1_actors_palette_number);

	/* Draw UI */
	draw_sprite(&ui_sheet, "stage_announce", 174, 100, ui_palette_number);

	/* Health bar background */
	draw_sprite(&ui_sheet, "barbg_left",  10, 10, 0);
	for(int x = 0; x < 100; ++x)
		draw_sprite(&ui_sheet, "barbg_mid", 15+x, 10, 0);
	draw_sprite(&ui_sheet, "barbg_right", 115, 10, 0);

	/* Draw animated health bar inside */
	float const s = sin(0.001f * timer_ms_gettime64());
	uint8_t health_count = 15 + 70 * s*s;

	draw_sprite(&ui_sheet, "healthbar_left", 16, 18, 0);
	for(int x = 0; x < health_count; ++x)
		draw_sprite(&ui_sheet, "healthbar_mid", 16+5 + x, 18, 0);
	draw_sprite(&ui_sheet, "healthbar_right",   16+5 + health_count, 18, 0);


	/* Draw animated enemies */
	uint8_t sprite_number = (unsigned)(0.01f * timer_ms_gettime64()) % 8;
	char sprite_name[32];
	snprintf(sprite_name, sizeof(sprite_name), "mage_combat%u", sprite_number);
	sprite_name[31] = 0;
	draw_sprite(&stage1_actors_sheet, sprite_name, 100, 300, stage1_actors_palette_number);

	snprintf(sprite_name, sizeof(sprite_name), "mage_shadowform%u", sprite_number);
	sprite_name[31] = 0;
	draw_sprite(&stage1_actors_sheet, sprite_name, 250, 300, stage1_actors_palette_number);

	snprintf(sprite_name, sizeof(sprite_name), "mage_idle%u", sprite_number);
	sprite_name[31] = 0;
	draw_sprite(&stage1_actors_sheet, sprite_name, 350, 300, stage1_actors_palette_number);

	pvr_scene_finish();
}

int main(int argc, char *argv[]) {
	int result = 0;

	if(init()) {
		puts("Cannot init");
		result = 1;
		goto cleanup;
	}

	for(;;) {
		draw();
	}

cleanup:
	spritesheet_free(&stage1_actors_sheet);
	spritesheet_free(&ui_sheet);

	return result;
}
